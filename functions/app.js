const config = {
    apiKey: "AIzaSyASBD7MZwJrvcm0IkkZv_TmTztL2zoGhq0",
    authDomain: "picolonetbot.firebaseapp.com",
    databaseURL: "https://picolonetbot.firebaseio.com",
    projectId: "picolonetbot",
    storageBucket: "picolonetbot.appspot.com",
    messagingSenderId: "35813951438"
};

const firebase = require('firebase/app');
require('firebase/database');
firebase.initializeApp(config);

const Slimbot = require('slimbot');
const slimbot = new Slimbot('612943859:AAGhVRvJAzk3yHCcyaFhc5na-yl1stDHYGg');

// Register listeners
slimbot.on('message', message => {
    // reply when user sends a message
    console.log(message);
    if (message.new_chat_members) {
        addNewMembers(message);
    }
    if (message.left_chat_member) {
        removeMember(message);
    }
    //slimbot.sendMessage(message.chat.id, 'Message received');
});

function addNewMembers(message) {
    let newMembers = message.new_chat_members;
    newMembers.forEach(function(member) {
        firebase.database().ref('telegram/picolonet/members/' + member.id).set({
            member
        }, function(error) {
            if (error) {
                console.error(error);
            } else {
                // Data saved successfully!
            }
        });
    });
}

function removeMember(message) {
    let member = message.left_chat_member;
    firebase.database().ref('telegram/picolonet/members/' + member.id).remove();     
}

// Call API
slimbot.startPolling();